﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ZodiacoFinal.Startup))]
namespace ZodiacoFinal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
