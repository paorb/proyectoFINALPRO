
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 08/05/2015 11:22:55
-- Generated from EDMX file: c:\users\yu.castro\documents\visual studio 2013\Projects\ZodiacoFinal\ZodiacoFinal\BDZodiaco.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [FinalZodiaco];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'EmpresaSet'
CREATE TABLE [dbo].[EmpresaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Mision1] nvarchar(max)  NOT NULL,
    [Vision1] nvarchar(max)  NOT NULL,
    [Detalles] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'categoriaSet'
CREATE TABLE [dbo].[categoriaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre_catg] nvarchar(max)  NOT NULL,
    [Descripcion_catg] nvarchar(max)  NOT NULL,
    [adicional] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'EmpresaSet'
ALTER TABLE [dbo].[EmpresaSet]
ADD CONSTRAINT [PK_EmpresaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'categoriaSet'
ALTER TABLE [dbo].[categoriaSet]
ADD CONSTRAINT [PK_categoriaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------